(function($){

var PathAnimation = function($el, opt) {
	var defaultOpt = {
		startAt: 0,  // integer
		endAt: 0,    // integer - y coord on path
		speed: 1,	 // integer
		path: null,  // dom node
		onScrollEnd: function() {},
		onAnimation: function() {}
	};
    this.opt = $.extend(true, defaultOpt, opt || {});
    this.opt.pathHeight = this.opt.path.getBoundingClientRect().height;
    this.opt.pathLength = this.opt.path.getTotalLength();
    this.opt.pathEnd = this.opt.path.getPointAtLength(this.opt.pathLength).y;
    this.$el = $el;
	this.animate()
}

PathAnimation.prototype.animate = function(){
    var opt = this.opt;
    var $el = this.$el;
    var lastScrollTop = 0;
	var currentScrolledElementPos = 0;
	var animationEndedPos = 0;

	var unitToPixelsRatio = opt.pathHeight / opt.pathLength;
	var currentPercent = 0;
	var duration = 2;
	var animationRequestId = null;
	$(window).on('scroll', function() {
		var scrollDown = true;
		var scrollTop = $(this).scrollTop();

		// TO RETHINK
		var scrollPos = scrollTop - opt.startAt;
		// var targetPercent = scrollPos / opt.pathLength;
		var targetPercent = Math.min(scrollPos / (opt.pathLength * unitToPixelsRatio), 1);
		targetPercent = targetPercent * opt.speed;
		// END RETHINK

		// events sent back to callback methods
		var event = {
			done: targetPercent,
			scrollTop: scrollTop,
			$el: $el,
			// elOffset: Math.round($el[0].getBBox().y + ($el[0].getBBox().height / 2))
		};

		// if (event.elOffset !== 0) {
		// 	currentScrolledElementPos = event.elOffset;
		// }

		// // scroll direction
		// if (scrollTop < lastScrollTop){
		// 	scrollDown = false;
		// }
		// lastScrollTop = scrollTop;

		// // if scroll pos did not reach starting point of animation
		// // set percent to 0 and wait
		// if (scrollTop <= opt.startAt) {
		// 	percent = 0;
		// 	// if (percent < 0) {
		// 	// }
		// }
		// // end event when reaching end of line and still scrolling down
		// if (currentScrolledElementPos >= opt.pathEnd && scrollTop >= animationEndedPos || 
		//  	opt.endAt > 0 && scrollPos >= opt.endAt && scrollDown
		// ) {
		// 	animationEndedPos = scrollTop;
		// 	percent = 1;
		// 	opt.onScrollEnd.call($el, event);
		// 	return;
		// } else {
		// 	// animation running
		// 	opt.onAnimation.call($el, event);
		// }

		// if (opt.endAt > 0 && scrollTop > opt.endAt) {
		// 	return;
		// }
		// 
		var pathPoint = targetPercent * opt.pathLength;
		var pathPosition = opt.path.getPointAtLength(pathPoint);
		$el[0].setAttribute('cx', pathPosition.x);
		$el[0].setAttribute('cy', pathPosition.y);

		// return;

		var startTimestamp = Date.now();
		var goToNewPosition = function() {
			var timestamp = Date.now();
			var animPercent = Math.min((timestamp - startTimestamp) / duration, 1);

			currentPercent = (targetPercent - currentPercent) * animPercent + currentPercent;
			

			if (animPercent < 1) {
				animationRequestId = requestAnimationFrame(goToNewPosition);
			}
		};


		//cancelAnimationFrame(animationRequestId);
		goToNewPosition();
	});
};

$.fn.PathAnimation =  function(opt) {
	new PathAnimation($(this), opt);
}

})(jQuery);
